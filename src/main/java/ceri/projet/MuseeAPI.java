package ceri.projet;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;


import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;


public class MuseeAPI extends AsyncTask<Void, String, Bitmap> {

    private MainActivity mainActivity;
    MuseeAPI(){
    }

    @Override
    protected Bitmap doInBackground(Void... voids) {
        try {
            JSONRespHandlerMusee.downloadCatalogue(mainActivity);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    MuseeAPI(MainActivity activity){
        this.mainActivity = activity;
    }
    protected void onPostExecute(Bitmap result) {
        if(mainActivity!=null)
            mainActivity.actualiser();
    }


}
