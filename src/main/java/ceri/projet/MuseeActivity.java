package ceri.projet;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;

public class MuseeActivity extends AppCompatActivity {
    private Musee obj;
    private TextView name;
    private TextView brand;
    private TextView description;
    private TextView working;
    private TextView details;
    private TextView timeFrame;
    private TextView year;
    private ImageView image;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.objet_activity);
        Musee obj = (Musee)getIntent().getSerializableExtra("objet");
        brand = findViewById(R.id.brand);
        description = findViewById(R.id.description);
        working = findViewById(R.id.working);
        details = findViewById(R.id.details);
        timeFrame = findViewById(R.id.decennies);
        year = findViewById(R.id.annee);
        name = findViewById(R.id.name);
        image = findViewById(R.id.photo);
        brand.setText(obj.getBrand());
        name.setText(obj.getName());
        description.setText(obj.getDescription());
        working.setText((obj.getWorking())?"oui":"non");
        details.setText(obj.getTechnicalDetailsString());
        timeFrame.setText(obj.getTimeFrameString());
        year.setText(obj.getYear()+"");
        new AfficherImage().execute("https://demo-lia.univ-avignon.fr/cerimuseum/items/"+obj.getId()+"/thumbnail");
    }

    class AfficherImage extends AsyncTask<String, Void, Bitmap> {
        protected Bitmap doInBackground(String... url) {
            try {
                System.out.println(url[0]);
                InputStream inputStream = new URL(url[0]).openStream();
                return BitmapFactory.decodeStream(inputStream);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        protected void onPostExecute(Bitmap result) {
            System.out.println("here "+(result==null));
            if(result!=null)
                image.setImageBitmap(result);
        }
    }
}
