package ceri.projet;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class Musee implements Serializable {

    public static final String TAG = Musee.class.getSimpleName();

    private String id;
    private String nom;
    private String[] categories;
    private String description;
    private long[] timeFrame;
    private long year;
    private String brand;
    private boolean working;
    private String[] technicalDetails;
    private String photo;

    public Musee(String id) {
        this.id = id;
    }

    public Musee(String name, String[] league) {
        this.nom = name;
        this.categories= league;
    }

    public Musee(String id, String nom, String[] categories, String description, long[] timeFrame, long year, String brand, boolean working, String[] technicalDetails, String photo) {
        this.id = id;
        this.nom = nom;
        this.categories = categories;
        this.description = description;
        this.timeFrame = timeFrame;
        this.timeFrame = timeFrame;
        this.year = year;
        this.brand = brand;
        this.working = working;
        this.technicalDetails = technicalDetails;
        this.photo = photo ;

    }

    public Musee(long aLong, String string, long aLong1, String string1, long aLong2, String string2, String string3, String string4) {
    }


    public String getId() {
        return id;
    }


    public String getName() {
        return nom;
    }

    public void setName(String nom) {
        this.nom = nom;
    }

    public String[] getCategories() {
        return categories;
    }

    public void setCategories(String[] categories) {
        this.categories = categories;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long[] getTimeFrame() {
        return timeFrame;
    }
    public String getTimeFrameString() {
        if(timeFrame==null) return "";
        String timeFrame = "";
        for(long annee: this.timeFrame){
            timeFrame += annee + ",";
        }
        return timeFrame;
    }
    public String getTechnicalDetailsString() {
        if(technicalDetails==null)return "";
        String timeFrame = "";
        for(String annee: this.technicalDetails){
            timeFrame += annee + ",";
        }
        return timeFrame;
    }
    public void setTimeFrame(long[] timeFrame) {
        this.timeFrame = timeFrame ;
    }

    public long getYear() {
        return year;
    }

    public void setYear(long year) {
        this.year = year;
    }



    public String getBrand() {
        if(brand==null) return "";
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public boolean getWorking() {
        return working;
    }

    public void setWorking(boolean working) {
        this.working = working;
    }

    public String[] getTechnicalDetails() {
        return technicalDetails;
    }

    public void setTechnicalDetails(String[] technicalDetails) {
        this.technicalDetails = technicalDetails;
    }
    public void setTechnicalDetailsString(String technicalDetails) {
        String[] details = technicalDetails.split(",");
        this.technicalDetails = new String[details.length];
        int i=0;
        for(String detail: details){
            try{
                this.technicalDetails[i++] = detail;
            }
            catch (Exception e){

            }
        }
    }
    public void setTimeFrameString(String timeFrame) {
        String[] years = timeFrame.split(",");
        this.timeFrame = new long[years.length];
        int i=0;
        for(String year: years){
            try{
                long yearLong = Long.parseLong(year);
                this.timeFrame[i++] = yearLong;
            }
            catch (Exception e){

            }
        }

    }
    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {this.photo = photo ;}

}
