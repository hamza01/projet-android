package ceri.projet;



import android.app.Activity;
import android.util.JsonReader;
import android.util.Log;
import android.widget.Switch;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class JSONRespHandlerMusee {


    private static String CATALOGUE_URL = "https://demo-lia.univ-avignon.fr/cerimuseum/catalog";
    private static final String TAG = JSONRespHandlerMusee.class.getSimpleName();


    public static void downloadCatalogue(Activity activity) throws IOException {
        URL url = new URL(CATALOGUE_URL);
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
        MuseeDbHelper museeDbHelper = new MuseeDbHelper(activity);
        String Json="";
        String line;
        while ((line = br.readLine()) != null) {
            Json += line;
        }
        try {
            JSONObject jsonObject = new JSONObject(Json);
            Iterator<String > ids = jsonObject.keys();
            while(ids.hasNext()){
                String id = ids.next();
                JSONObject museeJson = jsonObject.getJSONObject(id);
                Musee musee = new Musee(id);
                String name = museeJson.getString("name");
                musee.setName(name);
                String description = museeJson.getString("description");
                int year=0;
                String brand=null, working=null;
                try{
                    year = museeJson.getInt("year");
                }
                catch(Exception e){

                }

                try{
                    brand = museeJson.getString("brand");
                }
                catch(Exception e){

                }
                try{
                    brand = museeJson.getString("brand");
                }
                catch(Exception e){

                }
                try{
                    working = museeJson.getString("working");
                }
                catch(Exception e){

                }

                JSONArray technicalDetailsJSON=null,timeFrameJSON=null;
                try{
                    technicalDetailsJSON = museeJson.getJSONArray("technicalDetails");
                }
                catch(Exception e){

                }
                try{
                    timeFrameJSON = museeJson.getJSONArray("timeFrame");
                }
                catch(Exception e){

                }
                musee.setWorking((working!=null && working.equals("true")));
                musee.setBrand(brand);
                musee.setDescription(description);
                musee.setYear(year);
                if(technicalDetailsJSON!=null){
                    String[] technicalDetails = new String[technicalDetailsJSON.length()];
                    for(int i=0;i<technicalDetailsJSON.length();i++){
                        technicalDetails[i] = technicalDetailsJSON.getString(i);
                    }
                    musee.setTechnicalDetails(technicalDetails);
                }
                if(timeFrameJSON!=null){
                    long[] timeFrame = new long[timeFrameJSON.length()];
                    for(int i=0;i<timeFrameJSON.length();i++){
                        timeFrame[i] = timeFrameJSON.getLong(i);
                    }
                    musee.setTimeFrame(timeFrame);
                }

                museeDbHelper.addObjet(musee);
            //    System.out.println(musee.getName());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


}
