package ceri.projet;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

import static android.database.sqlite.SQLiteDatabase.CONFLICT_IGNORE;

public class MuseeDbHelper extends SQLiteOpenHelper {



    private static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "cerimusee.db";


    public static MuseeDbHelper dbHelper = null;
    public MuseeDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        dbHelper = this;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        final String SQL_CREATE_OBJET_TABLE= "CREATE TABLE MUSEE"+
                " ( _id INTEGER PRIMARY KEY," +
                "ID TEXT," +
                "NAME TEXT," +
                "DESCRIPTION TEXT," +
                "technicalDetails TEXT," +
                "timeFrame TEXT," +
                "YEAR INTEGER," +
                "BRAND TEXT," +
                "WORKING INTEGER);";

        db.execSQL(SQL_CREATE_OBJET_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
    public boolean addObjet(Musee musee){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("ID", musee.getId());
        cv.put("NAME", musee.getName());
        cv.put("DESCRIPTION", musee.getDescription());
        cv.put("technicalDetails", musee.getTechnicalDetailsString());
        cv.put("timeFrame", musee.getTimeFrameString());
        cv.put("YEAR", musee.getYear());
        cv.put("WORKING", musee.getWorking());
        cv.put("BRAND", musee.getBrand());
        db.insert("MUSEE",null,cv);
        return true;
    }
    public Cursor fetchAllObjets() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query("MUSEE", null,
                null, null, null, null, null, null);

        Log.d("h", "call fetchAllTeams()");
        if (cursor != null) {
            cursor.moveToFirst();
        }
        return cursor;
    }

    public Musee cursorToMusee(Cursor cursor){
       Musee musee = new Musee(cursor.getString(1));
       musee.setName(cursor.getString(2));
        musee.setBrand(cursor.getString(7));
       musee.setDescription(cursor.getString(3));
       musee.setYear(cursor.getInt(6));
       musee.setTechnicalDetailsString(cursor.getString(4).replace(',','\n'));
       musee.setTimeFrameString(cursor.getString(5));
        musee.setWorking(cursor.getInt(8)==1);

       return musee;
    }



}
