package ceri.projet;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {

    MuseeDbHelper museeDB ;
    Musee musee ;
    Cursor c ;
    ListView rv ;
    SimpleCursorAdapter adapter ;
    String table ;
    private ArrayList<Musee> objets = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        museeDB =new MuseeDbHelper(this);
        Cursor c = museeDB.fetchAllObjets();
        Musee m = null;
        while(!c.isAfterLast()){
            m = new MuseeDbHelper(this).cursorToMusee(c);
            System.out.println(m.getName()+" "+m.getDescription());
            objets.add(m);
            c.moveToNext();
        }
        if(objets.size()==0){
            System.out.println("Size "+objets.size());
            new MuseeAPI(this).execute();
        }

        else
            System.out.println("Size "+objets.size());
        actualiser();
    }
    public void actualiser(){
        Cursor c= new MuseeDbHelper(this).fetchAllObjets();
        c.moveToFirst();
        //dbHelper.populate();
        adapter =  new SimpleCursorAdapter(this,
                android.R.layout.simple_list_item_2,
                c,
                new String[] { "NAME", "BRAND"},
                new int[] { android.R.id.text1, android.R.id.text2});
        System.out.println((rv==null));
        rv = findViewById(R.id.listeteam);

        rv.setAdapter(adapter);
        rv.invalidateViews();
        Musee m = null;
        while(!c.isAfterLast()){
            m = new MuseeDbHelper(this).cursorToMusee(c);
            System.out.println(m.getName()+" "+m.getDescription());
            objets.add(m);
            c.moveToNext();
        }
        rv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Musee m = objets.get(position);
                Intent myIntent = new Intent(MainActivity.this, MuseeActivity.class);
                System.out.println("hhhh "+m.getName());
                myIntent.putExtra("objet", m);
                startActivity(myIntent);
            }
        });
    }


}
